<?php

namespace app\controllers;

use Yii;
use app\models\Alquileres;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AlquileresController implements the CRUD actions for Alquileres model.
 */
class AlquileresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alquileres models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Alquileres::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Alquileres model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Alquileres model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Alquileres();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigoAlquiler]);
        }

        $usuarios = \app\models\Usuarios::find()->all();
        $usuarios = \yii\helpers\ArrayHelper::map($usuarios, "codigoUsuario","nombre");
    
        $coches = \app\models\Coches::find()->select(["codigoCoche","CONCAT_WS(' ',codigoCoche,marca) marca"])->all();
        $coches = \yii\helpers\ArrayHelper::map($coches,"codigoCoche","marca");
        
       
        return $this->render('create', [
            'model' => $model
       
        ]);
    }

    /**
     * Updates an existing Alquileres model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigoAlquiler]);
        }

        $usuarios = \app\models\Usuarios::find()->all();
        $usuarios = \yii\helpers\ArrayHelper::map($usuarios, "codigoUsuario","nombre");
    
        $coches = \app\models\Coches::find()->select(["codigoCoche","CONCAT_WS(' ',codigoCoche,marca) marca"])->all();
        $coches = \yii\helpers\ArrayHelper::map($coches,"codigoCoche","marca");
        
        return $this->render('update', [
            'model' => $model
         
        ]);
    }

    /**
     * Deletes an existing Alquileres model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alquileres model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Alquileres the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alquileres::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionPruebas(){
        $listado = Alquileres::findOne(6);//select el registro de alquileres numero 6
        var_dump($listado->coche0->marca);// con el registro del alquiler podemos mostrar todos os datos del vehiculo con el modelo coche0 que ya está relacionado con el alquiler
        
    }
    public function actionAlquileresusuar($id){
        
        $dataProvider = new ActiveDataProvider([
           'query' => Alquileres::getAlquileresusuar($id) 
        ]);
        
        return $this->render("alquileresusuar",[
            'dataProvider'=>$dataProvider,
        ]);
    }  
    
    
        public function actionAlquileresyearfecha($id){
        
        $dataProvider = new ActiveDataProvider([
           'query' => Alquileres::getAlquileresYearFecha($id) 
        ]);
        
        return $this->render("alquileresyearfecha",[
            'dataProvider'=>$dataProvider
        ]);
    } 
      public function actionAlquilerescoche($id){
       //accediendo al registro dinamicamente
          
        $registro = Alquileres::findOne($id);
  
        $dataProvider = new ActiveDataProvider([
           'query' => $registro->getAlquileresCoche($id) 
        ]);
        
        return $this->render("alquilerescoche",[
            'dataProvider'=>$dataProvider
        ]);
    } 
    
        
    
    public function actionAlquileresmarca($id){
       //accediendo al registro dinamicamente
          
        $registro = Alquileres::findOne($id);
  
        $dataProvider = new ActiveDataProvider([
           'query' => $registro->coche0->getAlquileresMarca() 
        ]);
        
        return $this->render("alquileresmarca",[
            'dataProvider'=>$dataProvider
        ]);
    } 
    
    
        public function actionAlquileresunamarca($id){
       //accediendo al registro dinamicamente
          
        $registro = Alquileres::findOne($id);
  
        $dataProvider = new ActiveDataProvider([
           'query' => $registro->coche0->getAlquileresunamarca() 
        ]);
        
        return $this->render("alquileresmarca",[
            'dataProvider'=>$dataProvider
        ]);
    } 
    
    
    
    
//    public function actionAlquileresusuar($id){
//        
//        $dataProvider = new ActiveDataProvider([
//            'query' => Alquileres::find()
//                        ->where("usuario=$id"), 
//        ]);
//        
//        
//         return $this->render('alquileresusuar', [
//            'dataProvider' => $dataProvider,
//         
//        ]);
//        
//    }
}
