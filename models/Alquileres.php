<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alquileres".
 *
 * @property int $codigoAlquiler
 * @property int $usuario
 * @property int $coche
 * @property string $fecha
 *
 * @property Coches $coche0
 * @property Usuarios $usuario0
 */
class Alquileres extends \yii\db\ActiveRecord
{
    
    public $campoyear; // creamos este atributo para guardar el año de la fecha. El modelo necesita este nuevo hueco para guardar ese valor.
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alquileres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario', 'coche'], 'integer'],
            [['fecha'], 'safe'],
            [['usuario', 'coche', 'fecha'], 'unique', 'targetAttribute' => ['usuario', 'coche', 'fecha']],
            [['coche'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::className(), 'targetAttribute' => ['coche' => 'codigoCoche']],
            [['usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario' => 'codigoUsuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoAlquiler' => 'Codigo Alquiler',
            'usuario' => 'Usuario',
            'coche' => 'Coche',
            'fecha' => 'Fecha de Alquiler',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoche0()
    {
        return $this->hasOne(Coches::className(), ['codigoCoche' => 'coche']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(Usuarios::className(), ['codigoUsuario' => 'usuario']);
    }
    public function afterFind() {
           parent::afterFind();
           //el afterfind es como un disparador de sql. Se pueden crear atributos y pasarle valores para utilizarlos en el resto de la aplicacion
           //ejemplo de como añadir el año de cada fecha seleccionada. Previamente hay que crear el atributo campoyear en el modelo alquileres.
           //$this->campoyear= Yii::$app->formatter-asDate($this->fecha, 'php:Y');
           $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y');//formateamos las fechas cuando se muestran en las selecciones
           
    }
    public function beforeSave($insert) {
        if(parent::beforeSave($insert)){
           $this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)->format("Y/m/d");// antes de grabar pasar de d/m/Y a Y/m/d o formato de mysql
           return true;
        }
    }
 
    
    public static function listar() {
        //es estatico pq usamos :: y esto significa que se llama desde dentro de la clase
        // si la funcion es estatica se usa self y si es dinamico o normal se usa $this->
        return self::find()->all();
           
        
    }
    //obtener un drowdownlist de usuarios y coches de manera dinamica
    public static function getUsuarios(){
        $usuarios = Usuarios::find()->all();
        $usuarios = \yii\helpers\ArrayHelper::map($usuarios, "codigoUsuario","nombre");
        return $usuarios;
    }
    
    public function getCoches(){
        $coches = Coches::find()->select(["codigoCoche","CONCAT_WS(' ',codigoCoche,marca) marca"])->all();
        $coches = \yii\helpers\ArrayHelper::map($coches,"codigoCoche","marca");
        return $coches;
    }
    // es static cuando queremos acceder a toda la tabla. Sacar todos los datos.
    //obtener un drowdownlist de usuarios y coches de manera estatica
    
//    public static function getUsuarios(){
//        $usuarios = Usuarios::find()->all();
//        $usuarios = \yii\helpers\ArrayHelper::map($usuarios, "codigoUsuario","nombre");
//        return $usuarios;
//    }
//    
//    public function getCoches(){
//        $coches = Coches::find()->select(["codigoCoche","CONCAT_WS(' ',codigoCoche,marca) marca"])->all();
//        $coches = \yii\helpers\ArrayHelper::map($coches,"codigoCoche","marca");
//        return $coches;
//    }
    public static function getAlquileresusuar($id){
        
        return self::find()
                ->where("usuario=$id");
        
    }
    
   public static function getAlquileresYearFecha($id){
        
        //$regAlquiler = self::findOne()->where("Codigoalquiler=$id");
       // $registro=self::findOne($id);//con esta subconsulta obtenemos los datos del registro de alquiler psado por parametro
       //sacamos el año de la fecha para evitar el formato de mysql 
        $registro = self::find()//select Year(fecha) from Alquileres where CodigoAlquiler = $id (Esta consulta haria esto)
                ->select("YEAR(fecha) campoyear")
                ->where("codigoAlquiler = $id")
               // ->asArray()//se puede utilizar para guardar el resultado de la consulta cuando por ejemplo no queremos crear un atributo nuevo en el modelo para guardar este valor de año fecha.
                ->one();
        //$anno = $registro["anno"];// De esta forma recogeriamos el resultado de la selec devuelto en un array
        $anno = $registro->campoyear;
       
        
        return self::find()
                ->select('*,YEAR(fecha) campoyear')
                ->where("YEAR(fecha)=$anno");//all() ejecuta ya la consulta .Esta consulta hace select * from alquileres where year(fecha)  = $anno
        
    }
    
    public function getAlquileresCoche(){
        
        $dato = $this->coche;
       return self::find()
                ->where("coche = $dato");
               // ->asArray()//se puede utilizar para guardar el resultado de la consulta cuando por ejemplo no queremos crear un atributo nuevo en el modelo para guardar este valor de año fecha.
               
 
    }
    
    
    public static function getAlquileresuncoche($id){
        return self::find()
                ->where("coche=$id");
    }
    public static function getAlquileresunamarca(){
         $marca = $this->coche0->marca; 
        return self::find()>joinWith('coche0',false)
                ->where("marca = '$marca'");
    }
    
    
    
    public function getAlquileresMarca(){
       $marca = $this->coche0->marca; 
  
       return Alquileres::find()->joinWith('coche0',false)
                ->where("marca = '$marca'");
           
               // ->asArray()//se puede utilizar para guardar el resultado de la consulta cuando por ejemplo no queremos crear un atributo nuevo en el modelo para guardar este valor de año fecha.
               
 
    }
    
    
}
