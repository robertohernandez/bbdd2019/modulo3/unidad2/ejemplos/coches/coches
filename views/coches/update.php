<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Coches */

$this->title = 'Update Coches: ' . $model->codigoCoche;
$this->params['breadcrumbs'][] = ['label' => 'Coches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoCoche, 'url' => ['view', 'id' => $model->codigoCoche]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coches-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
