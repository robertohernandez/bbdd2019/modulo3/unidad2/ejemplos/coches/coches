<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coches-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Coches', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'codigoCoche',
             [
                'attribute'=>'codigoCoche',//si el atributo existe lo podemos poner pq ordena por el campo
                'label'=>'Coche',
                'format'=>'raw',// raw es ideal para fotos , enlaces
                'content'=>function($model){
                  return html::a('Ver alquileres del Coche '.$model->codigoCoche,['coches/cochesalquileres','id'=>$model->codigoCoche],
                          ['class'=>'btn btn-large']);
                }
            ],                     
            
            
           // 'marca',
                [
                'attribute'=>'marca',//si el atributo existe lo podemos poner pq ordena por el campo
                'label'=>'Marca',
                'format'=>'raw',// raw es ideal para fotos , enlaces
                'content'=>function($model){
                  return html::a('Ver alquileres de la marca '.$model->marca,['coches/marcaalquileres','id'=>$model->marca],
                          ['class'=>'btn btn-large']);
                }
            ],                     
            'color',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
