<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alquileres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alquileres', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
 
 
 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           
            'codigoAlquiler', 
            'usuario',
//            'usuario0.nombre',//manera de mostrar el nombre del usuario ademas de su codigo
            [
                'label'=>'Nombre del Usuario',
                'format'=>'raw',
                'content'=>function($model){
                  return html::a($model->usuario0->nombre,['alquileres/alquileresusuar','id'=>$model->usuario],
                          ['class'=>'btn btn-large']);//controlador y accion que muestre todos los alquileres de este usuario enviado el parametro del usuario
                  //en el enlace primero se pasa el campo nombre y luego el controlador con la accion y por ultimo el parametro que se le pasa
                }
            ],
            //'coche',
            [
                'attribute'=>'coche',//si el atributo existe lo podemos poner pq ordena por el campo
                'label'=>'Marca',
                'format'=>'raw',// raw es ideal para fotos , enlaces
                'content'=>function($model){
                  return html::a('Ver alquileres del coche '.$model->coche,['alquileres/alquilerescoche','id'=>$model->coche],
                          ['class'=>'btn btn-large']);
                }
            ],              
                    
                    
            //'coche0.marca',//manera de mostrar la marca del vehiculo ademas de su codigo
            [
                'attribute'=>'marca',//si el atributo existe lo podemos poner pq ordena por el campo
                'label'=>'Marca',
                'format'=>'raw',// raw es ideal para fotos , enlaces
                'content'=>function($model){
                  return html::a('Ver alquileres de la marca '.$model->coche0->marca,['alquileres/alquileresmarca','id'=>$model->codigoAlquiler],
                          ['class'=>'btn btn-large']);
                }
            ],                     
                    
                    
                    
//            'fecha',
            [
                'label'=>'Fecha de Alquiler',
                'format'=>'raw',// raw es ideal para fotos , enlaces
                'content'=>function($model){
                  return html::a('Ver alquileres del año de la fecha '.$model->fecha,['alquileres/alquileresyearfecha','id'=>$model->codigoAlquiler],
                          ['class'=>'btn btn-large']);
                }
            ],        
                    

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
