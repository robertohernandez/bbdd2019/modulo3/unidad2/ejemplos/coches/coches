<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alquileres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-index">

    <div class="jumbotron">
        <h1>Estos son los alquileres del coche <?= $dataProvider->models[0]->coche ?></h1>
        <p><?= 
            html::a(
            "Volver",
            Yii::$app->request->referrer,
            [
                'class' => 'btn btn-primary'
            ]
           )
        ?></p>
    </div>

   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'codigoAlquiler', 
            'usuario',
            'coche',       
            'fecha',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
