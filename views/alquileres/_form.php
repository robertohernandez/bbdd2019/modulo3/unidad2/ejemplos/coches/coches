<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Alquileres */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alquileres-form">
 // sacamos todos los usuarios.Este equivale a un select * from usuarios
 <?php
//    $usuarios = \app\models\Usuarios::find()->all();
//    $usuarios = \yii\helpers\ArrayHelper::map($usuarios, "codigoUsuario","nombre");
//    $coches = \app\models\Coches::find()->select(["codigoCoche","CONCAT_WS(' ',codigoCoche,marca) marca"])->all();
//    $coches = \yii\helpers\ArrayHelper::map($coches,"codigoCoche","marca");
    //var_dump($coches);
  
 ?>
   <?php $form = ActiveForm::begin(); ?>
 


   <?= $form->field($model, 'usuario')->dropDownList($model->usuarios) ?>

   <?= $form->field($model, 'coche')->dropDownList($model->coches) ?>

 <!--Recibiendo registros con funcion estatica
 
    //$form->field($model, 'usuario')->dropDownList(app\models\Usuarios::getUsuarios()) 

    //$form->field($model, 'coche')->dropDownList(\app\models\Coches::getCoches()) 

  -->
<!--         opcion 1
    echo '<label class="control-label">'.$model->getAttributeLabel("fecha").'</label>';
    echo DatePicker::widget([
         'model' => $model, 
         'attribute' => 'fecha',
         'options' => ['placeholder' => 'Introduce Fecha ...'],
         'pluginOptions' => [
            'todayHighlight' => true,
            'todayBtn' => true,
            'format' => 'yyyy/m/dd',
            'autoclose' => true,
        ]
    ]);
    ?>
    
    opcion 2 utilizando label directamente desde el DatePicker
                 -->
    <?php
        echo $form->field($model, 'fecha')->widget(DatePicker::classname(), [
        
        'options' => ['placeholder' => 'Introduce Fecha ...'],
         'pluginOptions' => [
            'todayHighlight' => true,
            'todayBtn' => true,
            'format' => 'dd/mm/yyyy',
            'autoclose' => true,
        ]
        ]);
        
?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
